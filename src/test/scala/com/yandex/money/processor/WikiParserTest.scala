package com.yandex.money.processor

import com.yandex.money.Suite

import scala.collection.JavaConversions._

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
class WikiParserTest extends Suite {

    test("parse paragraph with plain text") {
        val xml = loadFirstXmlTag("/paragraph.xml")
        val res = parser.parseParagraph(xml)
        println(res)
    }

    test("parse paragraph with span") {
        val xml = loadFirstXmlTag("/paragraph_span.xml")
        val res = parser.parseParagraph(xml)
        println(res)
    }

    test("parse paragraph with rich test") {
        val xml = loadFirstXmlTag("/paragraph_rich.xml")
        val res = parser.parseParagraph(xml)
        println(res)
    }

    test("parse \"ul\" listing") {
        val xml = loadFirstXmlTag("/listing_ul.xml")
        val res = parser.parseListing(xml)
        println(res)
    }

    test("parse code macro") {
        val xml = loadFirstXmlTag("/macro_code.xml")
        val res = parser.parseMacro(xml)
        println(res)
    }

    test("parse table") {
        val xml = loadFirstXmlTag("/table.xml")
        val res = parser.parseTable(xml)
        println(res.header)
        println(res.body)
    }

    test("parse link with empty body") {
        val xml = loadFirstXmlTag("/link_empty.xml")
        val res = parser.parseLink(xml)
        res.text shouldBe None
    }

    test("parse ac:link") {
        val xml = loadFirstXmlTag("/link_ac.xml")
        val res = parser.parseLink(xml)
        println(res)
    }

    test("parse internal \"a\" link") {
        val xml = loadFirstXmlTag("/link_a.xml")
        val res = parser.parseLink(xml)
        println(res)
    }

    test("parse section") {
        val xml = loadFirstXmlTag("/section.xml")
        val res = parser.parseSection(xml)
        print(res)
    }

    test("parse image") {
        val xml = loadFirstXmlTag("/image.xml")
        val res = parser.parseImage(xml)
        print(res)
    }

    test("parse span") {
        val xml = loadFirstXmlTag("/span.xml")
        val res = parser.parseSeq(Seq(xml))
        print(res)
    }

    test("parse macro note") {
        val xml = loadFirstXmlTag("/macro_text.xml")
        val macroTag = parser.parseMacro(xml)
        val serializedMacro = serializer.serializeMacro(macroTag)
        val output = xmlOutputter.outputString(Seq(serializedMacro))
        println(output)
    }

    test("parse table with pre tag") {
        val xml = loadFirstXmlTag("/table_pre_column.xml")
        val macroTag = parser.parseTable(xml)
        val serializedMacro = serializer.serializeTable(macroTag)
        val output = xmlOutputter.outputString(Seq(serializedMacro))
        println(output)
    }
}
