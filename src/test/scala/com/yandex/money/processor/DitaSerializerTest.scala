package com.yandex.money.processor

import com.yandex.money.Suite
import com.yandex.money.model.{Column, Content, Macro, Paragraph, Table, Text, XmlNode}

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
class DitaSerializerTest extends Suite {

    test("serialize paragraph") {
        val p = Paragraph(Content(List(Text("foo"))))
        val out = serializer.serializeParagraph(p)
        prettyPrint(Seq(out))
    }

    test("serialize table") {

        def entry(string: String) = Column(createSingletonContent(createParagraph(string)), None,
            None)

        val header = Seq(entry("header1"), entry("header2"))
        val body = Seq(
            Seq(entry("row1_col1"), entry("row1_col2")),
            Seq(entry("row2_col1"), entry("row2_col2"))
        )
        val table = Table(header, body)
        val out = serializer.serializeTable(table)
        prettyPrint(Seq(out))

    }

    test("serialize macro(info)") {
        val macroTag = Macro(Macro.Note, text("Note about something"), None)
        val out = serializer.serializeMacro(macroTag)
        prettyPrint(Seq(out))
    }

    test("serialize macro(code)") {
        val xml = loadFirstXmlTag("/macro_code.xml")
        val macroTag = parser.parseMacro(xml)
        val res = serializer.serializeMacro(macroTag)
        prettyPrint(Seq(res))
    }

    test("serialize paragraph with span") {
        val xml = loadFirstXmlTag("/paragraph_span.xml")
        val p: Content = parser.parseParagraph(xml)
        val res = serializer.serializeContent(p)
        prettyPrint(res)
    }

    test("serialize macro(php)") {
        val xml = loadFirstXmlTag("/macro_php.xml")
        val macroTag = parser.parseMacro(xml)
        val res = serializer.serializeMacro(macroTag)
        prettyPrint(Seq(res))
    }

    test("serialize span") {
        val xml = loadFirstXmlTag("/span.xml")
        val span = parser.parseSeq(Seq(xml))
        val res = serializer.serialize(span)
        prettyPrint(res)
    }

    test("serialize image") {
        val xml = loadFirstXmlTag("/image.xml")
        val image = parser.parseImage(xml)
        val res = serializer.serializeImage(image)
        prettyPrint(Seq(res))
    }

    private def text(str: String) = Content(List(Text(str)))

    private def createParagraph(text: String) = Paragraph(Content(List(Text(text))))

    private def createSingletonContent(node: XmlNode) = Content(List(node))

}
