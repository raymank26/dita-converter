package com.yandex.money

import java.io.{StringReader, StringWriter}
import java.nio.file.{Path, Paths}

import com.yandex.money.Suite._
import com.yandex.money.processor.utils.{LinkResolver, StaticManagerTrait, WikiDataProviderTrait}
import com.yandex.money.processor.{DitaSerializer, WikiParser}
import com.yandex.money.xml.CustomXmlOutputProcessor

import org.jdom2.input.SAXBuilder
import org.jdom2.output.{Format, XMLOutputter}
import org.jdom2.{Content, Document, Element}
import org.scalatest.{FunSuite, Matchers}

import scala.collection.JavaConverters._
import scala.io.Source

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
class Suite extends FunSuite with Matchers {

    lazy val linkResolver = {
        val sampleFile = "/foo/bar.xml"
        LinkResolver(Paths.get("/foo/bar"), Map().withDefault { _ => sampleFile },
            Map().withDefault { _ => sampleFile }, allowLinkErrors = true)
    }

    lazy val wikiDataProvider = new MockWikiDataProvider
    lazy val wikiParserBuilder = WikiParser
        .fromConfig(Paths.get("/static"), wikiDataProvider, linkResolver)
        .setImageManager(new MockStaticManager)
    lazy val parser = wikiParserBuilder
        .setPageId(1)
        .setOutputPath(Paths.get("/sample/output.xml"))
        .setCurrentFileName("foobar.xml")
        .create()
    lazy val serializer: DitaSerializer = DitaSerializer
        .fromConfig(wikiDataProvider, linkResolver)
        .setPageId(1)
        .create()
    lazy val xmlOutputter = new XMLOutputter()

    final def loadFirstXmlTag(name: String): Element = {
        val f = (loadResource _)
            .andThen(Utils.unescapeHtml)
            .andThen(Utils.wrapNamespace)
            .andThen(new StringReader(_))
            .andThen(new SAXBuilder().build(_).getRootElement)
            .andThen(_.getChildren.get(0).detach())
            .andThen(Utils.trim)
        f(name)
    }

    final def addXmlHeader(rawXml: String) = {


    }

    final def prettyPrint(contents: Seq[Content]): Unit = {
        val encoding = "UTF-8"

        val outputter = new XMLOutputter(Format.getPrettyFormat
            .setEncoding(encoding), new CustomXmlOutputProcessor)
        val document = new Document(new Element("content").addContent(contents.asJava))
        val writer = new StringWriter()
        scala.util.control.Exception.ultimately(writer.close())(
            outputter.output(document, writer)
        )
        println(writer.toString)
    }

    private def loadResource(name: String): String = Source.fromURL(getClass.getResource(name))
        .mkString

}

object Suite {

    class MockWikiDataProvider extends WikiDataProviderTrait {

        override def getPageChildren(id: Int): Seq[Int] = Seq()

        override def loadResources(ids: Seq[Int], overwrite: Boolean, staticPath: Path): Unit = ()

        override def prepareWikiContent(pageId: Int, content: Element): Element = content

        override def getWikiPage(pageId: Int, cachePath: Path, overwrite: Boolean): Element = ???
    }

    class MockStaticManager extends StaticManagerTrait {

        override def addImage(name: String): StaticManagerTrait = this

        override def outputPath: Path = Paths.get("/tmp")

        override def merge(imageManager: StaticManagerTrait): StaticManagerTrait = this

        override def clearUnused(): Unit = ()

        override def usedNames: Set[String] = Set()
    }
}
