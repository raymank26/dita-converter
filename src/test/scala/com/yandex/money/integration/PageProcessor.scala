package com.yandex.money.integration

import com.yandex.money.Suite
import com.yandex.money.xml.XmlTransformer

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
class PageProcessor extends Suite {

    test("process document https://wiki.yamoney.ru/pages/viewpage.action?pageId=97603545") {
        confluenceToDita("/integration/97603545.xml")
    }

    test("process document https://wiki.yamoney.ru/pages/viewpage.action?pageId=100892970") {
        confluenceToDita("/integration/100892970.xml")
    }

    test("process document https://wiki.yamoney.ru/pages/editpage.action?pageId=93762368") {
        confluenceToDita("/integration/93762368.xml")
    }

    test("process document https://wiki.yamoney.ru/pages/editpage.action?pageId=93624518") {
        confluenceToDita("/integration/93624518.xml")
    }

    test("process document https://wiki.yamoney.ru/pages/editpage.action?pageId=93757941") {
        confluenceToDita("/integration/93757941.xml")
    }

    test("process document https://wiki.yamoney.ru/pages/editpage.action?pageId=93762669") {
        confluenceToDita("/integration/93762669.xml")
    }

    test("process document https://wiki.yamoney.ru/pages/editpage.action?pageId=95584776") {
        confluenceToDita("/integration/95584776.xml")
    }

    private def confluenceToDita(path: String): Unit = {
        val xml = loadFirstXmlTag(path)
        val xmlTransformer = new XmlTransformer(skipFirstTitle = true)
        val sectionizedXml = xmlTransformer.transform(xml)
        val ditaMarkup = parser.parse(sectionizedXml)
        val res = serializer.serialize(ditaMarkup)
        prettyPrint(res)
    }


}
