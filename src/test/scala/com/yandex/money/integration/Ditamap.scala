package com.yandex.money.integration

import com.yandex.money.Suite
import com.yandex.money.processor.DitamapProcessor

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
class Ditamap extends Suite {

    test("ditamap parser") {
        println(DitamapProcessor.load("/integration/ditamap.xml"))
    }

    test("collect ditamap information") {
        val ditamap = DitamapProcessor.load("/integration/ditamap.xml")
        val res = DitamapProcessor.collectInformation(ditamap)
        println(res)
        println(res.ids)
    }
}
