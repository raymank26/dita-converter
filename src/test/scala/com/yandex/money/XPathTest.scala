package com.yandex.money

import com.yandex.money.xml.WikiNamespaces

import org.jdom2.filter.Filters
import org.jdom2.xpath.XPathFactory

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
class XPathTest extends Suite {
    test("test sample XPath expression in macro node") {
        val xml = loadFirstXmlTag("/macro_php.xml")
        //        xml.detach()
        println(xml.getNamespacesInScope)
        //        prettyPrint(Seq(xml))
        val xpath = XPathFactory.instance()
        val expr = xpath.compile(".//ac:parameter[@ac:name=\"language\"]/@ac:name",
            Filters.attribute(), null, WikiNamespaces.ac)
        expr.evaluate(xml).size() should equal(1)
    }

}
