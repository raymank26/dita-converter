<content>
    <h1 class="confluence-link">Протокол приема платежей для интернет-магазинов</h1>
    <h2 class="confluence-link">О протоколе</h2>
    <p>Протокол разработан для интернет-магазинов и онлайн-сервисов, с его помощью можно произвести
        интеграцию с платежным решением Яндекс.Денег. 
    </p>
    <p>Вы можете начать разработку прямо сейчас — чтобы все настроить и проверить в 
        <ac:link>
            <ri:page ri:content-title="Тестовые данные"/>
            <ac:plain-text-link-body><![CDATA[тестовом режиме]]></ac:plain-text-link-body>
        </ac:link>
        . А чтобы принимать настоящие платежи, нужно подключить <a href="https://kassa.yandex.ru/">
            Яндекс.Кассу</a>: подать заявку на подключение, получить доступ в личный кабинет и
        заключить
        договор с компанией «Яндекс.Деньги». Сделать это могут только организации — юрлица или ИП.
    </p>
    <p>Руководство по работе с протоколом предназначено для разработчиков сайтов, к которым
        собираются
        подключать или уже подключили Яндекс.Кассу.
    </p>
    <h2>Что позволяет платежное решение</h2>
    <ul>
        <li>Принимать платежи за товары и услуги 
            <ac:link ac:anchor="paymenttypes">
                <ac:plain-text-link-body><![CDATA[разными способами]]></ac:plain-text-link-body>
            </ac:link>
            .
        </li>
        <li>Обрабатывать платежи <a
            href="https://wiki.yamoney.ru/pages/viewpage.action?pageId=93762657">
            автоматически или вручную</a>.
        </li>
        <li>Управлять платежами: повторять, откладывать и осуществлять возвраты (в рамках протокола
            управления платежами, 
            <ac:link>
                <ri:page ri:content-title="Управление заказами"/>
                <ac:plain-text-link-body><![CDATA[MWS]]></ac:plain-text-link-body>
            </ac:link>
            ).
        </li>
        <li>Получать ежедневные реестры переводов.</li>
    </ul>
    <h2>С чего начать и что дальше</h2>
    <ol>
        <li>
            <a href="https://money.yandex.ru/joinups">Подайте заявку на подключение</a> и получите
            доступ в
            личный кабинет.
        </li>
        <li>Заполните анкету, предоставьте необходимые документы и заключите договор.</li>
        <li>Прочитайте документацию.</li>
        <li>Выберите <a href="https://wiki.yamoney.ru/pages/viewpage.action?pageId=93624532">способ
            подключения</a>.
        </li>
        <li>Реализуйте данный протокол.</li>
    </ol>
    <p>Вот и все — можно принимать платежи.</p>
    <p>В этом руководстве рассматривается <a
        href="https://wiki.yamoney.ru/pages/viewpage.action?pageId=97603545">техническая сторона
        подключения</a>. Вопросы по регистрации в личном кабинете, по анкете и договору можно
        задавать
        менеджеру Кассы.
    </p>
    <h2>
        <ac:structured-macro ac:name="anchor">
            <ac:parameter ac:name="">paymenttypes</ac:parameter>
        </ac:structured-macro>
        Как вам будут платить
    </h2>
    <p>После подключения на вашем сайте можно будет платить разными способами:</p>
    <ul>
        <li>с банковской карты Visa, MasterCard и Maestro;</li>
        <li>из кошелька в Яндекс.Деньгах;</li>
        <li>из кошелька WebMoney;</li>
        <li>наличными через терминал — по коду;</li>
        <li>со счета мобильного телефона;</li>
        <li>через мобильный терминал (mPOS);</li>
        <li>через Сбербанк Онлайн;</li>
        <li>через «Альфа-Клик»;</li>
        <li>через интернет-банк Промсвязьбанка;</li>
        <li>через MasterPass (платежный сервис системы MasterCard, к нему можно привязать все
            банковские
            карты, чтобы потом при оплате не указывать их данные);
        </li>
        <li>через QIWI Wallet;</li>
        <li>через КупиВкредит (Тинькофф Банк);</li>
        <li>через Доверительный платеж («Куппи.ру»).</li>
    </ul>
    <p>Вы можете посмотреть, как выглядит оплата для ваших покупателей, в <a
        href="http://demokassa.ru/">демо-магазине</a>.
    </p>
    <p>С успешных платежей Яндекс.Деньги взимают комиссию — на сайте Кассы можно посмотреть <a
        href="https://kassa.yandex.ru/terms#fees">тарифы</a>. Не обязательно подключать все способы
        сразу — вы можете выбрать нужные при заключении договора.
    </p>
    <h2>Кто уже подключил Яндекс.Кассу</h2>
    <p>
        <ac:image>
            <ri:attachment ri:filename="image2015-6-23 19:20:24.png"/>
        </ac:image>
    </p>
    <p> </p>
    <h2>Кому задавать вопросы</h2>
    <p>Пишите менеджерам Кассы на 
        <a class="external-link" href="mailto:merchants@money.yandex.ru" rel="nofollow">merchants@
            <span
                class="s2">money</span>.<span class="s2">yandex</span>.<span class="s2">ru</span> 
        </a>
        .
    </p>
    <h2>Что читать дальше</h2>
    <p>
        <a href="https://wiki.yamoney.ru/pages/viewpage.action?pageId=97603545">Описание процесса
            подключения
        </a>
    </p>
    <p>
        <a href="https://wiki.yamoney.ru/pages/viewpage.action?pageId=93624532">Способы
            подключения
        </a>
    </p>
    <p>
        <a href="https://wiki.yamoney.ru/pages/viewpage.action?pageId=93624543">Платежная форма</a>
    </p>
    <p>
        <ac:link>
            <ri:page ri:content-title="HTTP-уведомления о переводах"/>
            <ac:plain-text-link-body>
                <![CDATA[HTTP-уведомления о переводах]]></ac:plain-text-link-body>
        </ac:link>
    </p>
    <p>
        <ac:link>
            <ri:page ri:content-title="Email-уведомления о переводах"/>
            <ac:plain-text-link-body>
                <![CDATA[Email-уведомления о переводах]]></ac:plain-text-link-body>
        </ac:link>
    </p>
</content>
