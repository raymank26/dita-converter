package com.yandex.money.model.ditamap

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
abstract class Topic(expandedByDefault: Boolean) extends Traversable[Topic]
