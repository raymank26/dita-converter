package com.yandex.money.model.ditamap

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class Topicgroup(title: String, topics: Seq[Topic], expandedByDefault: Boolean)
    extends Topic(expandedByDefault) {

    override def foreach[U](f: (Topic) => U): Unit = {
        f(this)
        topics.foreach(f)
    }

    override def toString(): String = s"Topicgroup($title, $topics)"
}
