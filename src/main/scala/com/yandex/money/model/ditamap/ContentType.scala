package com.yandex.money.model.ditamap

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
sealed trait ContentType

case object Reference extends ContentType

case object Concept extends ContentType
