package com.yandex.money.model.ditamap

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class Ditamap(topics: Seq[Topic], attributes: Map[String, String]) extends Traversable[Topic] {
    override def foreach[U](f: (Topic) => U): Unit = topics.foreach(f)
}