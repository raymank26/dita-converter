package com.yandex.money.model.ditamap

import com.yandex.money.processor.utils.Cut

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class Topicref(navtitle: String, pathHref: String, wikiId: Int, wikiName: Option[String],
                    skipFirstHeader: Boolean, contentType: ContentType, cut: Cut,
                    children: Seq[Topic], expandedByDefault: Boolean)
    extends Topic(expandedByDefault) {

    override def foreach[U](f: (Topic) => U): Unit = f(this)

    override def toString(): String = s"Topicref($pathHref, $wikiId, $wikiName)"
}
