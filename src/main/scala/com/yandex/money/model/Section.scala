package com.yandex.money.model

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class Section(title: Option[String], content: Content, anchor: Option[String], level: Int)
    extends XmlNode
