package com.yandex.money.model

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case object Empty extends XmlNode
