package com.yandex.money.model

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class Macro(macroType: Macro.Type, content: Content, language: Option[String]) extends XmlNode

object Macro {

    sealed trait Type

    case object Note extends Type

    case object Attention extends Type

    case object Code extends Type

    case object Tip extends Type

    case object Restriction extends Type

    case object Anchor extends Type

    case object Children extends Type

}

