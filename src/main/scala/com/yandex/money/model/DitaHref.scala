package com.yandex.money.model

import java.nio.file.Path

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
sealed trait DitaHref {
    override def toString: String
}

final class DitaPathHref private(val path: Path) extends DitaHref {
    def resolve(ditaPath: DitaPathHref): DitaPathHref =
        new DitaPathHref(path.resolve(ditaPath.path))

    override def toString: String = path.toString.replace("\\", "/")
}

object DitaPathHref {
    def apply(path: Path): DitaPathHref = new DitaPathHref(path)
}

final class IdentityHref private(path: String) extends DitaHref {
    override def toString: String = path
}

object IdentityHref {
    def apply(path: String): DitaHref = new IdentityHref(path)
}
