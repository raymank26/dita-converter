package com.yandex.money.model

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class Image(href: DitaHref) extends XmlNode
