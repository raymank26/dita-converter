package com.yandex.money.model

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class TextModifier(text: String, modifierType: TextModifier.Type) extends XmlNode

object TextModifier {

    sealed trait Type

    case object Code extends Type

    case object Strong extends Type

    case object Italic extends Type

}
