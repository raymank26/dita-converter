package com.yandex.money.model

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class Column(content: Content, colspanSpec: Option[(Int, Int)],
                  rowspan: Option[Int]) extends XmlNode
