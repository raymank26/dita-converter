package com.yandex.money.model

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class Link(href: DitaHref, linkType: Link.Type, text: Option[String]) extends XmlNode

object Link {

    sealed trait Type

    case object External extends Type

    case object Internal extends Type

}
