package com.yandex.money.model

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class Table(header: Seq[Column], body: Seq[Seq[Column]]) extends XmlNode {

    def rowsSize: Int = body.length

    def columnsSize: Int = body.map(_.length).foldLeft(0)(Math.max)

}

