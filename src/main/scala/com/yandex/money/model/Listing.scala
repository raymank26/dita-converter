package com.yandex.money.model

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class Listing(listType: Listing.Type, nodes: Seq[Content]) extends XmlNode {

}

object Listing {

    trait Type

    case object Ul extends Type

    case object Ol extends Type

}
