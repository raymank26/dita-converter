package com.yandex.money.model

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
case class Content(child: Seq[XmlNode]) extends XmlNode
