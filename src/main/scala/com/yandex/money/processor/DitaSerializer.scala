package com.yandex.money.processor

import com.yandex.money.implicits.JdomImplicits._
import com.yandex.money.model._
import com.yandex.money.processor.utils.{LinkResolver, WikiDataProviderTrait}

import org.jdom2.Element

import scala.collection.JavaConversions._

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
class DitaSerializer(currentPageId: Int, config: DitaSerializer.Config) {

    def serialize(content: Content): Seq[org.jdom2.Content] = {
        serializeContent(content)
    }

    private[processor] def serializeModifier(textModifier: TextModifier): Element = {
        val tagName = textModifier.modifierType match {
            case TextModifier.Code => "parmname"
            case TextModifier.Strong => "b"
            case TextModifier.Italic => "i"
        }
        new Element(tagName).setText(textModifier.text)
    }

    private[processor] def serializeParagraph(paragraph: Paragraph): Element = {
        new Element("p").addContent(serializeContent(paragraph.node)).preserve
    }

    private[processor] def serializeLink(link: Link): Element = {
        val scope = link.linkType match {
            case Link.External => Some("external")
            case Link.Internal => None
        }

        val element = new Element("xref")
            .setAttribute("href", link.href.toString)
            .setAttribute("scope", scope)

        link.text match {
            case Some(text) => element.setText(text)
            case None => element
        }
    }

    private[processor] def serializeListing(listing: Listing): Element = {
        def li(content: Seq[org.jdom2.Content]): Element = {
            new Element("li").setContent(content).preserve
        }
        val items = listing.nodes.map(content => li(serializeContent(content)))
        val tagName = listing.listType match {
            case Listing.Ol => "ol"
            case Listing.Ul => "ul"
        }
        val element = new Element(tagName)
        items.foldLeft(element) {
            _ addContent _
        }
    }

    private[processor] def serializeTable(table: Table): Element = {
        def colspec(colname: String, i: Int): Element = {
            new Element("colspec")
                .setAttribute("colname", colname)
                .setAttribute("colnum", (i + 1).toString)
        }

        def columnIdToString(id: Int) = "c" + id

        val colspecs = {
            1.until(table.columnsSize + 1)
                .map(columnIdToString).zipWithIndex
                .map { case (str, i) => colspec(str, i) }
        }

        def serializeRows(rows: Seq[Seq[Column]]): Seq[Element] = {
            rows.map { row =>
                new Element("row").addContent(row.map(serializeColumn))
            }
        }

        def serializeColumn(column: Column): Element = {
            val from: Option[String] = column.colspanSpec.map(_._1).map(columnIdToString)
            val to: Option[String] = column.colspanSpec.map(_._2).map(columnIdToString)

            val content = serializeContent(column.content)

            val entry = new Element("entry")
                .setAttribute("namest", from)
                .setAttribute("nameend", to)
                .setAttribute("morerows", column.rowspan)
                .addContent(content)

            // is content should be printed in one line?
            if (content.exists { !_.isInstanceOf[Element] }) {
                entry.preserve
            } else {
                entry
            }
        }

        val thead = new Element("thead").setContent(serializeRows(Seq(table.header)))
        val tbody = new Element("tbody").setContent(serializeRows(table.body))
        val tgroup = new Element("tgroup").setAttribute("cols", table.columnsSize.toString)
            .addContent(colspecs).addContent(thead).addContent(tbody)

        new Element("table").setAttribute("frame", "all").addContent(tgroup)
    }

    private[processor] def serializeText(text: Text): org.jdom2.Content = new org.jdom2.Text(
        text.content)

    private[processor] def serializeSection(section: Section): Element = {
        if (section.level == 1) {
            val sectionElement = new Element("section").setAttribute("id", section.anchor)
            val title = section.title.map { title => new Element("title").setText(title) }

            sectionElement.addContent(title).addContent(serializeContent(section.content))
        }
        else {
            dl(section.title, serializeContent(section.content), section.anchor)
        }
    }

    private[processor] def serializeContent(content: Content): Seq[org.jdom2.Content] =
        content.child.flatMap {
            case content: Content => serializeContent(content)
            case item => Seq(serializeInner(item))
        }

    private[processor] def serializeMacro(macroTag: Macro): org.jdom2.Content = {

        def serializeInformation(label: String) = {
            new Element("note").setAttribute("type", label)
                .addContent(serializeContent(macroTag.content))
        }

        def serializeCode(text: Text): String = text.content

        def serializeLanguage: String = macroTag.language.getOrElse("no-highlight")

        lazy val codeblock: Element = {
            val content = serializeCode(macroTag.content.child.head.asInstanceOf[Text])
                .replace("\t", "    ")
            new Element("codeblock").setAttribute("code-lang", serializeLanguage).setText(content)
        }

        macroTag.macroType match {
            case Macro.Children => getCurrentPageChildren
            case Macro.Code => codeblock
            case Macro.Anchor => new Element("i")
                .setAttribute("id", serializeContent(macroTag.content).head.getValue)
            case Macro.Note => serializeInformation("note")
            case Macro.Attention => serializeInformation("attention")
            case Macro.Tip => serializeInformation("tip")
            case Macro.Restriction => serializeInformation("restriction")
        }
    }

    private[processor] def serializeImage(image: Image): Element = {
        new Element("image").setAttribute("href", image.href.toString)
    }

    private def getCurrentPageChildren: Element = {
        val urls = config.wikiDataProvider.getPageChildren(currentPageId).map(link =>
            config.linkResolver.resolveById(currentPageId, link)).map { link =>
            Content(Seq(link))
        }
        serializeListing(Listing(Listing.Ul, urls))
    }

    private[processor] def serializeInner(xmlNode: XmlNode): org.jdom2.Content = {
        xmlNode match {
            case p: Paragraph => serializeParagraph(p)
            case sec: Section => serializeSection(sec)
            case table: Table => serializeTable(table)
            case listing: Listing => serializeListing(listing)
            case link: Link => serializeLink(link)
            case textModifier: TextModifier => serializeModifier(textModifier)
            case text: Text => serializeText(text)
            case macroTag: Macro => serializeMacro(macroTag)
            case image: Image => serializeImage(image)
            case Empty => new org.jdom2.Text("")
            case _ => throw new IllegalArgumentException(
                s"no such serializer for ${xmlNode.toString }")
        }
    }

    private def dl(title: Option[String], content: Seq[org.jdom2.Content],
                   anchor: Option[String]): Element = {
        val dl = new Element("dl")
        val dlentry = new Element("dlentry")
        val dt = new Element("dt").setAttribute("id", anchor).setText(title)
        val dd = new Element("dd").addContent(content)

        dl.addContent(
            dlentry
                .addContent(dt)
                .addContent(dd))
    }
}

object DitaSerializer {

    def fromConfig(wikiDataProvider: WikiDataProviderTrait, linkResolver: LinkResolver): Builder = {
        new DitaSerializer.Builder(Config(wikiDataProvider, linkResolver))
    }

    class Builder(config: Config) {
        private var pageId: Int = _

        def setPageId(pageId: Int) = {
            this.pageId = pageId
            this
        }

        def create(): DitaSerializer = {
            if (pageId == 0) {
                throw new IllegalStateException("pageId is 0")
            }
            new DitaSerializer(pageId, config)
        }
    }

    private case class Config(wikiDataProvider: WikiDataProviderTrait, linkResolver: LinkResolver)

}
