package com.yandex.money.processor

import com.yandex.money.Utils
import com.yandex.money.model.ditamap.{Concept, ContentType, Ditamap, Reference, Topic, Topicgroup, Topicref}
import com.yandex.money.processor.utils.{Cut, FromTableCut, IdentityCut}

import org.jdom2.input.SAXBuilder
import org.jdom2.{Attribute, Element, Namespace}

import scala.collection.JavaConverters._

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
object DitamapProcessor {

    private val EXPANDED_BY_DEFAULT = "expanded-by-default"
    private val OTHERPROPS = "otherprops"
    private val NAVTITLE = "navtitle"
    private val HREF = "href"

    def load(path: String): Ditamap = parse(Utils.trim(new SAXBuilder().build(
        getClass.getResourceAsStream(path)).getRootElement))

    def collectInformation[T](ditamap: Ditamap): DitaMetaInfoImpl = {
        ditamap.topics.map(topic => collectTopicInformation(topic))
            .reduce(DitaMetaInfoImpl.merge)
    }

    def serializeDitamap(ditamap: Ditamap): Element = {
        val xsiNamespace = Namespace.getNamespace("xsi",
            "http://www.w3.org/2001/XMLSchema-instance")
        val xsa10Namespace = Namespace.getNamespace("xsa10",
            "http://dita.oasis-open.org/architecture/2005/")

        def serializeAttribute(attr: (String, String)): Attribute = {
            val attribute = new Attribute(attr._1, attr._2)
            attr._1 match {
                case "noNamespaceSchemaLocation" =>
                    attribute.setNamespace(xsiNamespace)
                case "lang" =>
                    attribute.setNamespace(Namespace.XML_NAMESPACE)
                case _ =>
                    attribute
            }
        }
        val res = new Element("map").addContent(ditamap.topics.map(serializeTopic).asJava)
        res.addNamespaceDeclaration(xsiNamespace)
        res.addNamespaceDeclaration(xsa10Namespace)
        res.setAttributes(ditamap.attributes.map(serializeAttribute).asJavaCollection)
    }

    def serializeTopic(topic: Topic): Element = {
        topic match {
            case topicref: Topicref => serializeTopicref(topicref)
            case topicgroup: Topicgroup => serializeTopicgroup(topicgroup)
        }
    }

    def serializeTopicref(topicref: Topicref): Element = {
        val element = {
            val res = new Element("topicref")
                .setAttribute(NAVTITLE, topicref.navtitle)
                .setAttribute(HREF, topicref.pathHref)
            if (topicref.expandedByDefault) {
                res.setAttribute(OTHERPROPS, EXPANDED_BY_DEFAULT)
            } else {
                res
            }
        }

        if (topicref.children.nonEmpty) {
            element.addContent(topicref.children.map(serializeTopic).asJava)
        }
        else {
            element
        }
    }

    def serializeTopicgroup(topicgroup: Topicgroup): Element = {
        val navtitle = new Element("topichead")
            .setAttribute(NAVTITLE, topicgroup.title)
        val element = new Element("topicgroup")
            .addContent(navtitle)
            .addContent(topicgroup.topics.map(serializeTopic).asJava)
        if (topicgroup.expandedByDefault) {
            element.setAttribute(OTHERPROPS, EXPANDED_BY_DEFAULT)
        } else {
            element
        }
    }

    private def parse(node: Element): Ditamap = {
        val topics = node.getChildren.asScala.map(parseTopic)
        Ditamap(topics, node.getAttributes.asScala.map(str => (str.getName, str.getValue)).toMap)
    }

    private def collectTopicInformation[T](topic: Topic): DitaMetaInfoImpl = {
        topic match {
            case topicref: Topicref => DitaMetaInfoImpl.merge(DitaMetaInfoImpl.singular(topicref),
                collectTopicInformationSeq(topicref.children))
            case topicgroup: Topicgroup => collectTopicInformationSeq(topicgroup.topics)
        }
    }

    private def collectTopicInformationSeq[T](topic: Seq[Topic]) = {
        topic.foldLeft(DitaMetaInfoImpl.empty) { case (map, nextTopic) =>
            DitaMetaInfoImpl.merge(map, collectTopicInformation(nextTopic))
        }
    }

    private def parseTopic(node: Element): Topic = {
        node.getName match {
            case "topicref" => parseTopicref(node)
            case "topicgroup" => parseTopicgroup(node)
            case other => throw new IllegalArgumentException(s"no such tag parser = $other")
        }
    }

    private def parseTopicref(node: Element): Topicref = {
        val navtitle = node.getAttribute(NAVTITLE).getValue
        val href = node.getAttribute(HREF).getValue
        val wikiId = node.getAttribute("wiki-id").getValue.toInt
        val wikiName = Option(node.getAttribute("wiki-name").getValue)
        val skipFirstHeader = Option(node.getAttributeValue("skip-first-header"))
            .exists(asBoolean)

        val children = node.getChildren.asScala.map(parseTopic)

        val cut = Option(node.getAttributeValue("cut-type")) match {
            case Some("table") => FromTableCut
            case None => IdentityCut
            case ct => throw new IllegalStateException(s"cut type $ct is not supported")
        }

        val contentType = Option(node.getAttributeValue("page-type")) match {
            case None => Reference
            case Some("concept") => Concept
            case ct => throw new IllegalStateException(s"contentType $ct is not supported")
        }
        Topicref(navtitle = navtitle, pathHref = href, wikiId = wikiId, wikiName = wikiName,
            skipFirstHeader = skipFirstHeader, contentType = contentType, cut = cut,
            children = children, expandedByDefault = isExpandedByDefault(node))
    }

    private def asBoolean(str: String) = str match {
        case "true" => true
        case "false" => false
    }

    private def parseTopicgroup(node: Element): Topicgroup = {
        val title = node.getChild("topichead").getAttribute(NAVTITLE).getValue
        val topics = node.getChildren.asScala.tail.map(parseTopic)
        Topicgroup(title = title, topics = topics, expandedByDefault = isExpandedByDefault(node))
    }

    private def isExpandedByDefault(node: Element) = {
        Option(node.getAttributeValue("otherprops")) match {
            case Some(EXPANDED_BY_DEFAULT) => true
            case _ => false
        }
    }

    trait DitaMetaInfo {

        val ids: Seq[Int]
        val idToHref: Map[Int, String]
        val nameToHref: Map[String, String]
        val idToContentType: Map[Int, ContentType]
        val idToTitle: Map[Int, String]
        val idToCutType: Map[Int, Cut]
    }

    case class DitaMetaInfoImpl(topics: Seq[Topicref]) extends DitaMetaInfo {

        override lazy val ids = idToHref.keys.toSeq
        override lazy val idToHref: Map[Int, String] = idToTopic.mapValues(_.pathHref)
        override lazy val nameToHref: Map[String, String] =
            topics.filter(_.wikiName.isDefined).map { topic =>
                (topic.wikiName.get, topic.pathHref)
            }.toMap
        override lazy val idToContentType: Map[Int, ContentType] = idToTopic.mapValues(
            _.contentType)
        override lazy val idToTitle: Map[Int, String] = idToTopic.mapValues(_.navtitle)
        override lazy val idToCutType: Map[Int, Cut] = idToTopic.mapValues(_.cut)
        private lazy val idToTopic: Map[Int, Topicref] = topics
            .map(topic => (topic.wikiId, topic))
            .toMap
    }

    object DitaMetaInfoImpl {

        def empty: DitaMetaInfoImpl = DitaMetaInfoImpl(Seq())

        def singular(topicref: Topicref) = DitaMetaInfoImpl(Seq(topicref))

        def merge(a: DitaMetaInfoImpl, b: DitaMetaInfoImpl): DitaMetaInfoImpl =
            new DitaMetaInfoImpl(a.topics ++ b.topics)
    }

}
