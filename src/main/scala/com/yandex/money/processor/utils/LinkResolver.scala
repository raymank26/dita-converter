package com.yandex.money.processor.utils

import java.nio.file.{Path, Paths}

import com.yandex.money.model.{DitaHref, DitaPathHref, IdentityHref, Link}
import com.yandex.money.processor.utils.LinkResolver.Config

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
class LinkResolver protected(config: Config) extends LinkResolverTrait {

    override def resolve(pageId: Int, link: String): Link = {
        if (isInternal(pageId, link)) {
            resolveById(pageId, link)
        } else {
            Link(IdentityHref(link), Link.External, None)
        }
    }

    override def resolveById(pageId: Int, linkToId: Int): Link = {
        val filePath = getLinkFromHash(pageId, linkToId, config.idToHref)
        Link(relativize(config.idToHref(pageId), filePath), Link.Internal, None)
    }

    override def resolveAnchor(anchor: String, fileName: String): Link = {
        Link(IdentityHref(s"#${fileName.takeWhile(_ != '.') }/$anchor"), Link.Internal, None)
    }

    override def resolveByTitle(pageId: Int, title: String): DitaHref = {
        val filePath = getLinkFromHash(pageId, title, config.nameToHref)
        relativize(config.idToHref(pageId), filePath)
    }

    protected def isInternal(pageId: Int, link: String) = {
        link.startsWith("https://wiki")
    }

    protected def resolveById(pageId: Int, link: String): Link =
        resolveById(pageId, "pageId=(\\d+)".r.findFirstMatchIn(link).map(_.group(1)).get.toInt)

    private def getLinkFromHash[T](fromPageId: Int, key: T, hash: Map[T, String]): String = {
        hash.get(key) match {
            case Some(x) => x
            case None => if (config.allowLinkErrors) {
                "link-mismatch"
            } else {
                throwLinkNotFound(fromPageId, key.toString)
            }
        }
    }

    private def relativize(fromFile: String, toFile: String): DitaHref = {
        val fromDirectory = Paths.get(s"/$fromFile").getParent
        DitaPathHref(fromDirectory.relativize(Paths.get(s"/$toFile")))
    }

    private def throwLinkNotFound(pageId: Int, to: String) =
        throw new IllegalStateException(s"Link not found. From = $pageId, to = $to")
}

object LinkResolver {

    def apply(outputPath: Path, nameToHref: Map[String, String], idToHref: Map[Int, String],
              allowLinkErrors: Boolean): LinkResolver = {
        new LinkResolver(Config(outputPath, nameToHref, idToHref, allowLinkErrors))
    }

    case class Config(outputPath: Path, nameToHref: Map[String, String], idToHref: Map[Int, String],
                      allowLinkErrors: Boolean)

}
