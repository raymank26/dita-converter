package com.yandex.money.processor.utils

import java.nio.file.Path

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
trait StaticManagerTrait {

    def addImage(name: String): StaticManagerTrait

    def merge(imageManager: StaticManagerTrait): StaticManagerTrait

    def outputPath: Path

    def clearUnused(): Unit

    def usedNames: Set[String]
}
