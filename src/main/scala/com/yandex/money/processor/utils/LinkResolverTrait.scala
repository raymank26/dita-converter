package com.yandex.money.processor.utils

import com.yandex.money.model.{DitaHref, Link}

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
trait LinkResolverTrait {

    def resolve(pageId: Int, link: String): Link

    def resolveById(pageId: Int, linkToId: Int): Link

    def resolveAnchor(anchor: String, fileName: String): Link

    def resolveByTitle(pageId: Int, title: String): DitaHref
}
