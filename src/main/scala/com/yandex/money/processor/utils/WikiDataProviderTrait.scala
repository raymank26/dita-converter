package com.yandex.money.processor.utils

import java.nio.file.Path

import org.jdom2.Element

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
trait WikiDataProviderTrait {

    /**
     * Loads page attachments and saves them to static path directory.
     *
     * @param ids page ids
     * @param overwrite replace if set
     * @param staticPath output path
     */
    def loadResources(ids: Seq[Int], overwrite: Boolean, staticPath: Path): Unit

    /**
     * Downloads page from remote server.
     *
     * @param pageId confluence wiki page id
     * @param cachePath local path to save pages
     * @param overwrite discard saved pages in cache
     * @return prepared XML node (unused header is removed)
     */
    def getWikiPage(pageId: Int, cachePath: Path, overwrite: Boolean): Element

    /**
     * Obtains ids of children pages.
     *
     * @param id parent page id
     * @return collection of children ids
     */
    def getPageChildren(id: Int): Seq[Int]

    /**
     * Prepares content for futher processing. This method can be overridden if needed.
     * @param content obtained xml content
     * @return prepared content
     */
    def prepareWikiContent(pageId: Int, content: Element): Element
}
