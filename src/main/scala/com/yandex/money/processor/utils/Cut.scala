package com.yandex.money.processor.utils

import com.yandex.money.implicits.JdomImplicits._
import com.yandex.money.processor.WikiParser
import com.yandex.money.xml.WikiNamespaces

import org.jdom2.Element
import org.jdom2.filter.Filters
import org.jdom2.xpath.XPathFactory

import scala.collection.JavaConverters._

/**
 * Cuts XML content from XML document.
 *
 * @author Anton Ermak (ermak@yamoney.ru).
 */
sealed trait Cut {
    final def prepareContent(element: Element): Element = {
        new Element(WikiParser.RootTagName).addContent(cut(element).asJava)
    }

    protected def cut(element: Element): Seq[Element]
}

class XPathCut(expr: String) extends Cut {

    override protected final def cut(element: Element): Seq[Element] = {
        XPathFactory.instance()
            .compile(expr, Filters.element(), null, WikiNamespaces.ac)
            .evaluate(element)
            .asScala
            .map(_.detach())
    }
}

object FromTableCut extends XPathCut(".//ac:layout-cell[2]/*[position()>1]")

object IdentityCut extends Cut {
    override protected def cut(element: Element): Seq[Element] = {
        element.getDetachedChildren.asScala
    }
}
