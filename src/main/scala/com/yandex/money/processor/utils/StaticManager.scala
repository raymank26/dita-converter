package com.yandex.money.processor.utils

import java.io.File
import java.nio.file.Path

import com.typesafe.scalalogging.Logger

import org.slf4j.LoggerFactory

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
final class StaticManager private(val absoluteOutputPath: Path,
                                  override val usedNames: Set[String]) extends StaticManagerTrait {

    private val logger = Logger(LoggerFactory.getLogger(getClass))

    override def addImage(name: String): StaticManagerTrait = {
        new StaticManager(absoluteOutputPath, usedNames + name)
    }

    override def merge(imageManager: StaticManagerTrait): StaticManagerTrait = {
        require(absoluteOutputPath == imageManager.outputPath)
        new StaticManager(absoluteOutputPath, usedNames ++ imageManager.usedNames)
    }

    override def clearUnused(): Unit = {
        val files: List[File] = {
            val temp: Array[File] = new File(absoluteOutputPath.toString).listFiles()
            if (temp == null) {
                logger.warn( """No static directory found. You need to restart parser and set
                               |reload-static-images = "true"""".stripMargin.replace('\n', ' '))
                List()
            } else {
                temp.toList
            }
        }
        files.foreach { file =>
            if (!usedNames.contains(file.getName)) {
                logger.info(s"Deleting unused image ${file.getName }")
                file.delete()
            }
        }
    }

    override def outputPath: Path = absoluteOutputPath

}

object StaticManager {

    def apply(outputPath: Path) = {
        new StaticManager(outputPath, Set())
    }

}
