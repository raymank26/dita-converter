package com.yandex.money.processor.utils

import java.io.{FileOutputStream, StringReader}
import java.nio.file.{Files, Path}

import com.yandex.money.Utils
import com.yandex.money.processor.utils.WikiDataProvider.{Attachment, Config}

import org.jdom2.Element
import org.jdom2.input.SAXBuilder
import spray.json.DefaultJsonProtocol._
import spray.json.{JsArray, JsValue, JsonParser}

import scalaj.http.{Http, HttpOptions, HttpRequest, HttpResponse}

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
class WikiDataProvider protected(config: Config) extends WikiDataProviderTrait {

    private val logger = Utils.getLogger("WikiDataProvider")

    private val httpClient = HttpClient(config.cookies.map {
        case (key, value) => key + "=" + value
    }.mkString(","))

    /**
     * Loads page attachments and saves them to static path directory.
     *
     * @param ids page ids
     * @param overwrite replace if set
     * @param staticPath output path
     */
    override def loadResources(ids: Seq[Int], overwrite: Boolean, staticPath: Path): Unit = {
        ids.foreach { id =>
            logger.info(s"Start downloading attachments of page id = $id")
            getUrls(id).foreach(downloadAttachment(_, staticPath))
        }
    }

    /**
     * Downloads page from remote server.
     *
     * @param pageId confluence wiki page id
     * @param cachePath local path to save pages
     * @param overwrite discard saved pages in cache
     * @return prepared XML node (unused header is removed)
     */
    override def getWikiPage(pageId: Int, cachePath: Path, overwrite: Boolean): Element = {
        if (overwrite) {
            downloadWikiPage(pageId, cachePath)
        } else {
            loadWikiPageFromCache(WikiDataProvider.getNameFromId(pageId), cachePath) match {
                case Some(page) => page
                case None => downloadWikiPage(pageId, cachePath)
            }
        }
    }

    /**
     * Obtains ids of children pages.
     *
     * @param id parent page id
     * @return collection of children ids
     */
    override def getPageChildren(id: Int): Seq[Int] = {
        downloadJson(s"/content/$id/child/page")
            .asJsObject.fields("results")
            .convertTo[JsArray]
            .elements.map { item =>
            item.asJsObject.fields("id")
                .convertTo[String]
                .toInt
        }
    }

    /**
     * Prepares content for futher processing. This method can be overridden if needed.
     * @param content obtained xml content
     * @return prepared content
     */
    override def prepareWikiContent(pageId: Int, content: Element): Element = {
        config.idToCutType(pageId).prepareContent(content)
    }

    private def downloadWikiPage(pageId: Int, cachePath: Path): Element = {
        logger.info(s"Start downloading page wiki id = $pageId")

        val json = downloadJson(s"/content/$pageId?expand=body.storage").asJsObject.fields
        val pageContent = json("body")
            .asJsObject.fields("storage")
            .asJsObject.fields("value")
            .convertTo[String]

        val xmlContent = (Utils.wrapNamespace _)
            .andThen(Utils.unescapeHtml)
            .andThen(new StringReader(_))
            .andThen(new SAXBuilder().build(_))
            .andThen(_.getRootElement.detach())
            .apply(pageContent)

        val page = prepareWikiContent(pageId, xmlContent)
        Utils.saveXmlDocument(WikiDataProvider.getNameFromId(pageId), page, cachePath)
        page
    }

    private def loadWikiPageFromCache(filename: String, cachePath: Path): Option[Element] = {
        val file = cachePath.resolve(filename).toFile
        if (file.exists()) {
            val sax = new SAXBuilder()
            Some(sax.build(file).getRootElement)
        } else {
            None
        }
    }

    private def getUrls(pageId: Int): Seq[Attachment] = {
        val json = downloadJson(s"/content/$pageId/child/attachment")
        val elements = json
            .asJsObject.fields("results")
            .convertTo[JsArray]
            .elements
        elements.map { element =>
            val attachment = element.asJsObject.fields
            Attachment(Utils.normalizeResourceName(attachment("title").convertTo[String]),
                attachment("_links").asJsObject.fields("download").convertTo[String])
        }
    }

    private def downloadAttachment(attachment: Attachment, outputDirectory: Path): Unit = {
        val res = httpClient.getAsBytes(s"https://wiki.yamoney.ru${attachment.url }")
        checkStatusCode(res)
        val file = outputDirectory.resolve(attachment.name).toFile
        Files.createDirectories(outputDirectory)
        val writer = new FileOutputStream(file)
        writer.write(res.body)
        writer.close()
    }

    private def checkStatusCode(response: HttpResponse[_]) = {
        if (!response.is2xx) {
            //@formatter:off
            throw new IllegalStateException(s"""
               |Download error -  ${response.statusLine}. Try to update cookie in application.json
               |config file.""".stripMargin.replace('\n', ' '))
            //@formatter:on
        }
    }

    private def downloadJson(url: String): JsValue = {
        val response = httpClient.getAsString(s"https://wiki.yamoney.ru/rest/api$url")
        checkStatusCode(response)
        JsonParser(response.body)
    }
}

object WikiDataProvider {

    def apply(cookies: Seq[(String, String)], idToCutType: Map[Int, Cut]): WikiDataProvider =
        new WikiDataProvider(Config(cookies, idToCutType))

    private def getNameFromId(id: Int) = s"$id.xml"

    case class Config(cookies: Seq[(String, String)], idToCutType: Map[Int, Cut])

    private case class Attachment(name: String, url: String)

}

class HttpClient(cookies: String) {

    def getAsString(url: String): HttpResponse[String] = prepareHttpClient(url).asString

    def getAsBytes(url: String): HttpResponse[Array[Byte]] = prepareHttpClient(url).asBytes

    private def prepareHttpClient(url: String): HttpRequest = {
        Http(url)
            .header("Cookie", cookies)
            .timeout(HttpClient.TIMEOUT, HttpClient.TIMEOUT)
            .option(HttpOptions.allowUnsafeSSL)
    }

}

object HttpClient {

    private val TIMEOUT = 5 * 1000

    def apply(cookies: String) = new HttpClient(cookies)
}
