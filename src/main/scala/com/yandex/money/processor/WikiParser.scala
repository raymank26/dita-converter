package com.yandex.money.processor

import java.nio.file.Path

import com.yandex.money.Utils
import com.yandex.money.implicits.JdomImplicits._
import com.yandex.money.model._
import com.yandex.money.processor.WikiParser.RootTagName
import com.yandex.money.processor.utils.{LinkResolver, StaticManagerTrait, WikiDataProviderTrait}
import com.yandex.money.xml.WikiNamespaces
import com.yandex.money.xml.XmlTransformer.SkipTitleAttr

import org.jdom2.Content.CType
import org.jdom2.filter.Filters
import org.jdom2.xpath.XPathFactory
import org.jdom2.{Content => JdomContent, Element}

import scala.collection.JavaConverters._
import scala.collection.immutable.Nil

/**
 * The confluence parser. Builds model.
 * @author Anton Ermak (ermak@yaprocessor.ru).
 */
class WikiParser protected(currentPageId: Int, absoluteOutputPath: Path, currentFileName: String,
                           config: WikiParser.Config, var imageManager: StaticManagerTrait) {

    private val logger = Utils.getLogger(this.getClass.getCanonicalName)

    def parse(node: Element): Content = node.getName match {
        case RootTagName => parseSeq(node.getContent.asScala)
        case label => throw new IllegalStateException(
            s"root tag should be <$RootTagName>...</$RootTagName> but $label given")
    }

    private[money] def parseInner(content: JdomContent): XmlNode = {
        def parseElement(node: Element): XmlNode = node.getName match {
            case label if skip(node) => Empty
            case "section" => parseSection(node)
            case "p" => parseParagraph(node)
            case "ol" | "ul" => parseListing(node)
            case "structured-macro" => parseMacro(node)
            case "table" => parseTable(node)
            case "strong" | "code" | "em" | "pre" => parseTextModifier(node)
            case "a" | "link" => parseLink(node)
            case "image" => parseImage(node)
            case "span" | "u" | "div" => parseSeq(node.getContent.asScala)
            case label => throw new UnsupportedOperationException(s"no such node rule = $label")
        }

        content.getCType match {
            case CType.Text | CType.CDATA => parseText(content)
            case CType.Element => parseElement(content.asInstanceOf[Element])
            case label => throw new UnsupportedOperationException(s"no such node rule = $label")
        }
    }

    private def skip(node: Element): Boolean = node.getName match {
        case "br" => true
        case _ => false
    }

    private[processor] def parseSeq(nodes: Seq[JdomContent]): Content = {
        Content(nodes.map(parseInner))
    }

    private[processor] def parseTable(table: Element): Table = {

        def parseColumn(column: Element, columnIndex: Int): Column = {
            val colspan = Option(column.getAttributeValue("colspan")).map(_.toInt)
            val rowspan = Option(column.getAttributeValue("rowspan")).map(_.toInt - 1)
            val content = parseSeq(column.getContent.asScala)
            val colspec = colspan.map(length => (columnIndex, columnIndex + length - 1))
            Column(content, colspec, rowspan)
        }

        def parseRow(tr: Element): Seq[Column] = {
            if (tr.getName != "tr") {
                throw new IllegalStateException("is not \"tr\"")
            }
            tr.getChildren.asScala.zipWithIndex.map { case (node, index) =>
                parseColumn(node, index + 1)
            }
        }
        val theadChildren = Option(table.getChild("thead")).map(_.getChildren.asScala)
        val tbodyChildren = Option(table.getChild("tbody")).map(_.getChildren.asScala)
        val rawRows = (theadChildren, tbodyChildren) match {
            case (Some(a), Some(b)) => a ++ b
            case (Some(a), None) => a
            case (None, Some(b)) => b
            case _ => Seq()
        }

        val rows = rawRows.map(parseRow)
        new Table(rows.head, rows.tail)
    }

    private[processor] def parseImage(image: Element) = {
        val href = image.getChildren.get(0).getPrefixedAttributeValue("ri:filename").get
        val filename = Utils.normalizeResourceName(href)
        imageManager = imageManager.addImage(filename)
        val absoluteStaticOutputPath = config.imageHrefPrefix
            .resolve(filename)
        Image(DitaPathHref(absoluteOutputPath.relativize(absoluteStaticOutputPath)))
    }

    private[processor] def parseMacro(macroNode: Element): Macro = {

        logger.debug(s"macro: $macroNode")

        def getParameter(name: String): Option[String] = {
            val xpath = XPathFactory.instance().compile(
                s""".//ac:parameter[@ac:name="$name"]""",
                Filters.element(), null, WikiNamespaces.ac)
            Option(xpath.evaluateFirst(macroNode)).map(_.getValue)
        }

        def guessMessageType: Macro.Type = {
            getParameter("title").map {
                case "Внимание!" => Macro.Attention
                case "Ограничение" => Macro.Restriction
                case "Совет" => Macro.Tip
                case _ => Macro.Note
            } getOrElse Macro.Note
        }

        val macroType = macroNode.getPrefixedAttributeValue("ac:name").getOrElse("code") match {
            case "info" => guessMessageType
            case "anchor" => Macro.Anchor
            case "children" => Macro.Children
            case "code" => Macro.Code
            case other => throw new IllegalStateException(s"no such Macro.Type = $other")
        }

        val content = macroType match {
            case Macro.Anchor => Content(List(Text(macroNode.getValue)))
            case Macro.Children => Content(List())
            case _ =>
                val plainTextBody = macroNode.getPrefixedChild("ac:plain-text-body")
                if (plainTextBody.nonEmpty) {
                    Content(List(Text(plainTextBody.head.getText)))
                }
                else {
                    val richTextBody = macroNode.getPrefixedChild("ac:rich-text-body").get
                    parseSeq(richTextBody.getContent.asScala)
                }
        }

        new Macro(macroType, content, getParameter("language"))
    }

    private[processor] def parseParagraph(paragraphNode: Element): Content = {
        def isBr(node: JdomContent): Boolean = node match {
            case el: Element => el.getName == "br"
            case _ => false
        }
        def recursion(suffix: List[JdomContent], acc: Seq[JdomContent],
                      res: Seq[Paragraph]): Seq[Paragraph] = {
            suffix match {
                case Nil => res :+ Paragraph(parseSeq(acc))
                case head :: tl => if (isBr(head)) {
                    recursion(tl, Seq(), res :+ Paragraph(parseSeq(acc)))
                } else {
                    recursion(tl, acc :+ head, res)
                }
            }
        }
        val result = recursion(paragraphNode.getContent.asScala.toList, List(), List())
        if (result.length == 1) {
            Content(Seq(result.head))
        } else {
            Content(result)
        }
    }

    private[processor] def parseListing(listingNode: Element): Listing = {

        val listingType = listingNode.getName match {
            case "ol" => Listing.Ol
            case "ul" => Listing.Ul
            case other => throw new IllegalStateException(s"no such ListingNode.Type = $other")
        }

        val children = listingNode.getChildren("li").asScala.map { node =>
            parseSeq(node.getContent.asScala)
        }
        new Listing(listingType, children)
    }

    private[processor] def parseText(content: JdomContent): Text = {
        Text(content.getValue)
    }

    private[processor] def parseTextModifier(modifierNode: Element): TextModifier = {
        val modifierType = modifierNode.getName match {
            case "strong" => TextModifier.Strong
            case "code" | "pre" => TextModifier.Code
            case "em" => TextModifier.Italic
            case other => throw new IllegalStateException(
                s"no such ListingNode.Type = $other. pageId = $currentPageId")
        }
        new TextModifier(modifierNode.getValue, modifierType)
    }

    private[processor] def parseLink(linkNode: Element): Link = {
        logger.debug(linkNode.getValue)

        def parseLink: Link = {
            val text = linkNode.getPrefixedChild("ac:plain-text-link-body").map(_.getText)
            val anchor = linkNode.getPrefixedAttributeValue("ac:anchor")
            if (anchor.isDefined) {
                logger.debug("anchor is defined", anchor.get)
                config.linkResolver.resolveAnchor(anchor.get, currentFileName) copy (text = text)
            }
            else {
                val title = linkNode
                    .getChildren
                    .get(0)
                    .getPrefixedAttributeValue("ri:content-title").get
                if (title == null) {
                    throw new NullPointerException("link should have a title")
                }
                val filename = config.linkResolver.resolveByTitle(currentPageId, title)
                Link(filename, Link.Internal, text)
            }
        }

        def parseA: Link = {
            val link = linkNode.getAttribute("href").getValue
            val text = if (linkNode.getValue.isEmpty) None else Some(linkNode.getValue)

            config.linkResolver.resolve(currentPageId, link).copy(text = text)
        }

        linkNode.getName match {
            case "link" => parseLink
            case "a" => parseA
        }
    }

    private[processor] def parseSection(sectionNode: Element): Section = {
        val level = sectionNode.getAttribute("level").getValue.toInt
        val anchor = Option(sectionNode.getAttribute("anchor")).map(_.getValue)
        val content = parseSeq(sectionNode.getContent.asScala)
        val skipHeader: Boolean = sectionNode.getAttribute(SkipTitleAttr).getValue.toBoolean
        val header = skipHeader match {
            case true => None
            case false => Some(sectionNode.getAttribute("header").getValue)
        }

        Section(header, content, anchor, level)
    }
}

object WikiParser {

    val RootTagName = "content"

    def fromConfig(imageHrefPrefix: Path, wikiDataProvider: WikiDataProviderTrait,
                   linkResolver: LinkResolver): WikiParser.Builder = {
        new WikiParser.Builder(Config(imageHrefPrefix, wikiDataProvider, linkResolver))
    }

    case class Config(imageHrefPrefix: Path, wikiDataProvider: WikiDataProviderTrait,
                      linkResolver: LinkResolver)

    final class Builder(config: Config) {
        private var pageId: Int = _
        private var outputPath: Path = _
        private var filename: String = _
        private var imageManager: StaticManagerTrait = _

        def setPageId(pageId: Int): Builder = {
            this.pageId = pageId
            this
        }

        def setOutputPath(outputPath: Path): Builder = {
            this.outputPath = outputPath
            this
        }

        def setCurrentFileName(filename: String): Builder = {
            this.filename = filename
            this
        }

        def setImageManager(imageManager: StaticManagerTrait): Builder = {
            this.imageManager = imageManager
            this
        }

        def create(): WikiParser = new WikiParser(pageId, outputPath, filename, config,
            imageManager)
    }

}
