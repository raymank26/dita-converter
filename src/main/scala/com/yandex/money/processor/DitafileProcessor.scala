package com.yandex.money.processor

import com.yandex.money.model.ditamap.{Concept, ContentType, Reference}

import org.jdom2.{Content, Element, Namespace}

import scala.collection.JavaConversions._

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
object DitafileProcessor {

    def wrapByType(pageTitle: String, content: Seq[Content], pageType: ContentType,
                   pageDitaId: String): Element = {
        pageType match {
            case Reference => reference(pageTitle, content, pageDitaId)
            case Concept => concept(pageTitle, content, pageDitaId)
        }
    }

    private def wrapElement(el: String, id: String): Element = {
        val xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance")
        val root = new Element(el)
        root.addNamespaceDeclaration(xsi)
        root.addNamespaceDeclaration(Namespace.getNamespace("xsa4",
            "http://dita.oasis-open.org/architecture/2005/"))
        root.setAttribute("noNamespaceSchemaLocation",
            "urn:oasis:names:tc:dita:xsd:reference.xsd:1.1", xsi)
        root.setAttribute("id", id)
    }

    private def reference(title: String, content: Seq[Content], pageDitaId: String) = {
        val el = wrapElement("reference", pageDitaId)
        el.addContent(new Element("title").setText(title)).addContent(new Element("refbody")
            .addContent(content))
    }

    private def concept(title: String, content: Seq[Content], pageDitaId: String) = {
        val el = wrapElement("concept", pageDitaId)
        el.addContent(new Element("title").setText(title)).addContent(new Element("conbody")
            .addContent(content))
    }
}
