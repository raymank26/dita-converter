package com.yandex.money

import java.nio.file.Path

import com.yandex.money.Run.{FactoryService, PathProvider}
import com.yandex.money.config.{CommonConfig, DitamapConfig}
import com.yandex.money.processor.DitamapProcessor
import com.yandex.money.processor.DitamapProcessor.DitaMetaInfo
import com.yandex.money.processor.utils.{StaticManager, StaticManagerTrait}

/**
 * Converts full .ditamap file.
 *
 * @author Anton Ermak (ermak@yamoney.ru).
 */
object FullRun {

    def start(): Unit = {
        val ditamaps = DitamapConfig.load()
        val config = CommonConfig.load()

        ditamaps.items.foreach { ditamapItem =>
            val ditafile = DitamapProcessor.load(ditamapItem.file)
            val run = new Run {
                override val factoryService = new FactoryService {
                    override lazy val commonConfig: CommonConfig = config
                    override lazy val ditamapMeta: DitaMetaInfo = DitamapProcessor
                        .collectInformation(ditafile)
                    override val pathProvider: PathProvider = new PathProvider {
                        override val outputPath: Path = ditamapItem.output
                        override protected val staticPrefix: String = config.staticPrefix
                    }
                    override val imageManager: StaticManagerTrait =
                        StaticManager.apply(pathProvider.staticPath)
                }
            }
            Utils.saveXmlDocument(ditamapItem.name, DitamapProcessor.serializeDitamap(ditafile),
                ditamapItem.output, pretty = true)
            run.start()
        }
    }
}
