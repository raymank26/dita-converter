package com.yandex.money.config

import com.typesafe.config.ConfigFactory

import scala.collection.JavaConverters._

/**
 * Reads common config from application.conf
 *
 * @author Anton Ermak (ermak@yamoney.ru).
 */
object CommonConfig {

    def load(): CommonConfig = {
        val conf = ConfigFactory.load().getConfig("dita-converter")
        CommonConfig(conf.getBoolean("override"),
            conf.getObject("cookies")
                .unwrapped()
                .asInstanceOf[java.util.Map[String, String]]
                .asScala
                .toSeq,
            conf.getString("static-prefix"),
            conf.getBoolean("reload-static-images"))
    }

}

case class CommonConfig(overwrite: Boolean,
                        cookies: Seq[(String, String)],
                        staticPrefix: String,
                        reloadStaticImages: Boolean) {
}
