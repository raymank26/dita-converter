package com.yandex.money.config

import java.nio.file.{Path, Paths}

import com.typesafe.config.{ConfigFactory, ConfigObject}

import scala.collection.JavaConverters._

/**
 * Reads configuration from ditamaps.conf
 *
 * @author Anton Ermak (ermak@yamoney.ru).
 */
object DitamapConfig {

    def load(): DitamapConfig = {
        val config = ConfigFactory.load()

        val items = config.getList("dita-converter.ditamaps")
            .asScala
            .map { configValue =>
                val configObject = configValue.asInstanceOf[ConfigObject].toConfig
                DitamapItem(file = configObject.getString("file"),
                    output = Paths.get(configObject.getString("output")),
                    name = configObject.getString("name")
                )
            }
        DitamapConfig(items)
    }
}

case class DitamapConfig(items: Seq[DitamapItem])

case class DitamapItem(file: String, output: Path, name: String)
