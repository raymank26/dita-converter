package com.yandex.money.config

import java.nio.file.{Path, Paths}

import com.typesafe.config.{ConfigFactory, ConfigObject, ConfigValue}

import scala.collection.JavaConverters._

/**
 * Reads configuration from separate_pages.conf
 *
 * @author Anton Ermak (ermak@yamoney.ru).
 */
object SeparatePagesConfig {

    def load(): SeparatePagesConfig = {
        val conf = ConfigFactory.load().getConfig("dita-converter.separate-pages")

        val pages = conf.getList("list").asScala.map { pageConfig =>
            val pageObject = pageConfig.asInstanceOf[ConfigObject].toConfig
            val links = pageObject.getObject("links").asScala.toMap
                .map { (link: (String, ConfigValue)) =>
                    (link._1, link._2.unwrapped().toString)
                }
            (Page(pageObject.getInt("wiki-id"), pageObject.getString("path"),
                pageObject.getString("title"), pageObject.getString("belongs-to")), links)
        }
        val overallHash = pages.map(_._2).foldLeft(Map.empty[String, String]) { (result, links) =>
            result ++ links
        }
        SeparatePagesConfig(pages.map(_._1), overallHash,
            outputPath = Paths.get(conf.getString("output-path")))
    }
}

case class SeparatePagesConfig(pages: Seq[Page], links: Map[String, String], outputPath: Path)

case class Page(wikiId: Int, path: String, title: String, belongsTo: String)

