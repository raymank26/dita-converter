package com.yandex.money

import java.nio.file.{Path, Paths}

import com.typesafe.scalalogging.Logger
import com.yandex.money.Run.{FactoryService, PathProvider}
import com.yandex.money.config.CommonConfig
import com.yandex.money.processor.utils.{LinkResolver, StaticManagerTrait, WikiDataProvider}
import com.yandex.money.processor.{DitaSerializer, DitafileProcessor, DitamapProcessor, WikiParser}
import com.yandex.money.xml.XmlTransformer

import org.jdom2.{Content, Element}

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
trait Run {

    protected val factoryService: FactoryService
    private val logger: Logger = Utils.getLogger(getClass.getSimpleName)

    def start(): Unit = {
        val ditamapMeta = factoryService.ditamapMeta
        val wikiProvider = factoryService.wikiDataProvider
        val commonConfig = factoryService.commonConfig

        val wikiPages = fetchWikiPages(ditamapMeta.ids, wikiProvider, commonConfig.overwrite,
            factoryService.pathProvider.cachePath)

        fetchResourcesIfNeeded(wikiProvider, ditamapMeta.ids, commonConfig.reloadStaticImages,
            commonConfig.overwrite, factoryService.pathProvider.staticPath)

        val (ditaPages, imageManager) = convertWikiToDita(wikiPages, ditamapMeta, wikiProvider,
            factoryService.pathProvider)
        saveResultPages(ditaPages, ditamapMeta, factoryService.pathProvider.outputPath)
        imageManager.clearUnused()
    }

    private def fetchResourcesIfNeeded(wikiProvider: WikiDataProvider, ids: Seq[Int],
                                       reloadStaticImages: Boolean, overwrite: Boolean,
                                       staticPath: Path) = {
        if (reloadStaticImages) {
            wikiProvider.loadResources(ids, overwrite, staticPath)
        }
    }

    private def fetchWikiPages(ids: Seq[Int], wikiProvider: WikiDataProvider,
                               overwrite: Boolean, cachePath: Path): Seq[Element] = {
        ids.map { id =>
            wikiProvider.getWikiPage(id, cachePath, overwrite)
        }
    }

    private def convertWikiToDita(wikiPages: Seq[Element],
                                  meta: DitamapProcessor.DitaMetaInfo,
                                  wikiProvider: WikiDataProvider,
                                  pathProvider: PathProvider): (Seq[Seq[Content]], StaticManagerTrait) = {

        val ditaSerializerBuilder = factoryService.ditaSerializerBuilder

        val xmlTransformer = new XmlTransformer(skipFirstTitle = true)

        val res = wikiPages.zip(meta.ids).map { case (page, id) =>

            val transformed = xmlTransformer.transform(page.detach())
            logger.debug(s"start processing page $id")
            val wikiParser = factoryService.wikiParserBuilder
                .setPageId(id)
                .setOutputPath(pathProvider.outputPath.resolve(meta.idToHref(id)).getParent)
                .setCurrentFileName(Paths.get(meta.idToHref(id)).getFileName.toString)
                .setImageManager(factoryService.imageManager)
                .create()
            val parsed = wikiParser.parse(transformed)
                (ditaSerializerBuilder.setPageId(id).create().serialize(parsed), wikiParser
                    .imageManager)
        }
        val imageManager = res
            .map { _._2 }
            .reduce { _.merge(_) }
        (res.map { _._1 }, imageManager)
    }

    private def saveResultPages(ditaPages: Seq[Seq[Content]], maps: DitamapProcessor.DitaMetaInfo,
                                outputPath: Path) = {
        ditaPages.zip(maps.ids) foreach {
            case (page, id) =>
                val filename = maps.idToHref(id)
                val pagePath = Paths.get(outputPath.toString, filename)
                    .getFileName
                    .toString
                    .split("\\.")(0)
                logger.debug(s"path = ${pagePath.toString }")

                val wrapped = DitafileProcessor.wrapByType(maps.idToTitle(id), page,
                    maps.idToContentType(id), pagePath)
                Utils.saveXmlDocument(filename, wrapped, outputPath, pretty = true)
        }
    }
}

object Run {

    trait FactoryService {

        lazy val wikiParserBuilder: WikiParser.Builder =
            WikiParser.fromConfig(pathProvider.staticPath, wikiDataProvider, linkResolver)
        lazy val ditaSerializerBuilder: DitaSerializer.Builder =
            DitaSerializer.fromConfig(wikiDataProvider, linkResolver)
        lazy val wikiDataProvider: WikiDataProvider = WikiDataProvider(commonConfig.cookies,
            ditamapMeta.idToCutType)
        lazy val linkResolver: LinkResolver =
            LinkResolver(pathProvider.outputPath, ditamapMeta.nameToHref, ditamapMeta.idToHref,
                allowLinkErrors = false)

        val imageManager: StaticManagerTrait
        val ditamapMeta: DitamapProcessor.DitaMetaInfo
        val commonConfig: CommonConfig
        val pathProvider: PathProvider
    }

    trait PathProvider {
        lazy val cachePath: Path = outputPath.resolve("cache")
        lazy val staticPath: Path = outputPath.resolve(staticPrefix)
        val outputPath: Path
        protected val staticPrefix: String
    }

}
