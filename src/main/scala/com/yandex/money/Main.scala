package com.yandex.money

import scala.util.Try

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
object Main {

    def main(args: Array[String]): Unit = {
        Try(args(0)).toOption match {
            case Some("full") => FullRun.start()
            case None => SeparatePagesRun.start()
            case command => throw new IllegalArgumentException(
                s"""$command is not valid command.
                   |Try to use "full" for converting overall ditamap""".stripMargin)
        }
    }
}
