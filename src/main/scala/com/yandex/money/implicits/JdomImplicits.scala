package com.yandex.money.implicits

import java.util

import org.jdom2.{Attribute, Content, Element, Namespace}

import scala.collection.JavaConverters._

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
object JdomImplicits {

    /**
     * Stringify values.
     */
    private trait Read[T] {
        def asString(value: T): String
    }

    /**
     * Wrapper for [[Element]] that adds helper methods.
     *
     * @param el [[Element]] instance.
     */
    implicit class ElementEnhancement(el: Element) extends Element {

        def setAttribute[T: Read](name: String, value: Option[T]): Element = {
            value.foreach { foo => el.setAttribute(name, implicitly[Read[T]].asString(foo)) }
            el
        }

        def getDetachedChildren: util.List[Element] = {
            el.getChildren.asScala.toList.map(_.detach()).asJava
        }

        def getPrefixedChild(fullName: String): Option[Element] = {
            val (prefix, name) = makeSplit(fullName)
            el.getChildren.asScala.find { child =>
                child.getName == name && child.getNamespacePrefix == prefix
            }
        }

        def getPrefixedAttributeValue(fullName: String): Option[String] = {
            val (prefix, name) = makeSplit(fullName)
            val attr: Option[Attribute] = el
                .getAttributes
                .asScala
                .find { attr => attr.getName == name && attr.getNamespacePrefix == prefix }
            attr.map(_.getValue)
        }

        def addContent(content: Option[Content]) = {
            content match {
                case Some(value) => el.addContent(value)
                case None => el
            }
        }

        def setText(text: Option[String]) = {
            if (text.isDefined) {
                el.setText(text.get)
            }
            else {
                el
            }
        }

        /**
         * Sets attribute to element which marks that content should be printed in one line.
         * See [[com.yandex.money.xml.CustomXmlOutputProcessor]] for implementation details.
         *
         * @return modified element
         */
        def preserve = el.setAttribute("space", "preserve", Namespace.XML_NAMESPACE)

        private def makeSplit(fullName: String) = {
            val splits = fullName.split(":")
            (splits(0), splits(1))
        }
    }

    private object Read {
        implicit val intRead = new Read[Int] {
            override def asString(value: Int): String = value.toString
        }

        implicit val stringRead = new Read[String] {
            override def asString(value: String): String = value
        }
    }

}
