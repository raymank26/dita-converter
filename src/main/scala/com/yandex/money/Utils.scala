package com.yandex.money

import java.io.{BufferedReader, File, FileOutputStream, InputStreamReader}
import java.nio.channels.Channels
import java.nio.file.{Files, Path, Paths}

import com.typesafe.scalalogging.Logger
import com.yandex.money.xml.{CustomXmlOutputProcessor, WikiNamespaces}

import org.jdom2.output.{Format, XMLOutputter}
import org.jdom2.{Content, Document, Element}
import org.slf4j.LoggerFactory

/**
 * Utility functions for processing pages(saving, caching, etc.).
 *
 * @author Anton Ermak (ermak@yamoney.ru).
 */
object Utils {

    private val logger = getLogger("Utils")

    /**
     * Returns logger.
     *
     * @param name logger name.
     * @return logger.
     */
    def getLogger(name: String): Logger = {
        Logger(LoggerFactory.getLogger(name))
    }

    /**
     * Trims entire input. Keep in mind that the verbatim text is also trimmed.
     */
    def trim(node: Element): Element = node //scala.xml.Utility.trim(node)

    /**
     * Prepares raw XML content to Confluence Wiki namespace format.
     * @param rawXmlContent raw input
     * @return wrapped output
     */
    def wrapNamespace(rawXmlContent: String): String = {
        //@formatter:off
        s"""<ac:confluence
             |xmlns:${WikiNamespaces.toString(WikiNamespaces.ac)}
             |xmlns:${WikiNamespaces.toString(WikiNamespaces.ri)}
             |xmlns=\"\">$rawXmlContent</ac:confluence>""".stripMargin
        //@formatter:on
    }

    /**
     * Saved xml document to filesystem.
     *
     * @param filename filename.
     * @param page content.
     * @param outputPath path to save to.
     */
    def saveXmlDocument(filename: String, page: Content, outputPath: Path,
                        pretty: Boolean = false): Unit = {

        val filepath = Paths.get(outputPath.toString, filename)
        Files.createDirectories(filepath.getParent)

        val encoding = "UTF-8"

        val outputter = if (pretty) {
            new XMLOutputter(Format.getPrettyFormat
                .setEncoding(encoding), new CustomXmlOutputProcessor)
        } else {
            new XMLOutputter(Format.getRawFormat.setEncoding(encoding))
        }

        val writer = Channels.newWriter(new FileOutputStream(filepath.toString).getChannel,
            encoding)
        val document = new Document().addContent(page)

        scala.util.control.Exception.ultimately(writer.close())(
            outputter.output(document, writer)
        )
    }

    /**
     * Returns filename of file(specified by path) without extension suffix.
     *
     * @param path filepath.
     * @return filename without suffix.
     */
    def pagePathToDitaId(path: Path) = {
        logger.debug(s"path = ${path.toString }")
        path.getFileName.toString.split("\\.")(0)
    }

    /**
     * Cleans and normalizes wiki resource names.
     *
     * @param filename filename
     * @return
     */
    def normalizeResourceName(filename: String) = {
        val escapedChars = Seq('-', ' ', ':')
        escapedChars.foldLeft(filename)((prev, ch) => prev.replace(ch, '_'))
    }

    /**
     * Converts raw html to unescaped one.
     *
     * @param str input string.
     * @return unescaped string.
     */
    def unescapeHtml(str: String): String = {
        val symbols = Map(
            "&laquo;" -> "«",
            "&raquo;" -> "»",
            "&nbsp;" -> " ",
            "&larr;" -> "-",
            "&rarr;" -> "-",
            "&mdash;" -> "—",
            "&rdquo;" -> "”",
            "&ldquo;" -> "“",
            "&ndash;" -> "-"
        )
        symbols.foldLeft(str) { case (a, b) => a.replace(b._1, b._2) }
    }

    /**
     * Cleanup of static directory. This function check all static file using grep.
     *
     * TODO: refactor this one to more performance algorithm.
     *
     * @param staticPath static path.
     * @param outputPath output path.
     */
    def cleanupStaticFolder(staticPath: Path, outputPath: Path) = {
        if (staticPath.toFile.exists()) {
            new File(staticPath.toString).listFiles().foreach { file =>
                val command = s"""grep -rl ${file.getName } ${outputPath.toString }"""
                val grepProcess = Runtime.getRuntime.exec(command)
                grepProcess.waitFor()
                val reader = new BufferedReader(new InputStreamReader(grepProcess.getInputStream))
                val output = new StringBuffer
                var line = reader.readLine()
                while (line != null) {
                    output.append(line)
                    line = reader.readLine()
                }
                if (grepProcess.exitValue() != 0) {
                    file.delete()
                }
            }
        }
    }
}
