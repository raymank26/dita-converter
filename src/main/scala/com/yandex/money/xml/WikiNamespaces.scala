package com.yandex.money.xml

import org.jdom2.Namespace

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
object WikiNamespaces {

    val ac = Namespace.getNamespace("ac", "http://www.atlassian.com/schema/confluence/4/ac/")
    val ri = Namespace.getNamespace("ri", "http://www.atlassian.com/schema/confluence/4/ac/")

    def toString(namespace: Namespace) = {
        s"""${namespace.getPrefix }="${namespace.getURI }""""
    }

}
