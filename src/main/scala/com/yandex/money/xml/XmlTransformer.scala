package com.yandex.money.xml

import com.yandex.money.implicits.JdomImplicits._
import com.yandex.money.processor.WikiParser
import com.yandex.money.xml.XmlTransformer.SkipTitleAttr

import org.jdom2
import org.jdom2.Element

import scala.collection.JavaConverters._

/**
 * @author Anton Ermak (ermak@yamoney.ru).
 */
class XmlTransformer(skipFirstTitle: Boolean) {

    def transform(node: Element): Element = {
        val content = transformInner(children = prepareHeadersIfNeeded(node), acc = Seq(),
            currentLevel = 0)._1

        new Element(WikiParser.RootTagName)
            .addContent(skipTitleSection(content.head))
            .addContent(content.tail.asJava)
    }

    private def prepareHeadersIfNeeded(root: Element): List[Element] = {
        def process(node: Element): Element = {
            val header = node.getName match {
                case "h2" => new Element("h1")
                case "h3" => new Element("h2")
                case "h4" => new Element("h3")
                case other => node
            }
            header.addContent(node.getContent.asScala.toList.map(_.detach()).asJava)
        }

        val res = if (skipFirstTitle) {
            root.getChildren.asScala.toList.map { child =>
                process(child)
            }
        }
        else {
            root.getChildren.asScala
        }
        res.toList
    }

    private def skipTitleSection(section: Element): Element = {
        section.setAttribute(SkipTitleAttr, "true")
    }

    private def trySection(element: Element): Option[Int] = {
        if (element.getName.startsWith("h")) Some(element.getName.charAt(1).asDigit) else None
    }

    private def createSectionByLevel(level: Int, headerContent: Element,
                                     children: Seq[Element] = Seq()): Element = {
        val content = headerContent.getContent.asScala
        val header: String = content.filter(!isAnchor(_)).map(_.getValue).mkString
        val anchor: Option[String] = content.find(isAnchor).map(_.getValue)
        new Element("section")
            .setAttribute("header", header)
            .setAttribute("level", level.toString)
            .setAttribute("anchor", anchor)
            .setAttribute("skip-title", "false")
            .addContent(children.asJava)
    }

    private def isAnchor(node: jdom2.Content): Boolean = node match {
        case el: Element => el.getName == "structured-macro"
        case _ => false
    }

    private def transformInner(children: List[Element], acc: Seq[Element],
                               currentLevel: Int): (Seq[Element], List[Element]) = {
        if (children.isEmpty) {
            (acc, children)
        }
        else {
            trySection(children.head) match {
                case Some(level) =>
                    if (level > currentLevel) {
                        val (res, others) = transformInner(children.tail, Seq(), currentLevel + 1)
                        val section = createSectionByLevel(currentLevel + 1, children.head, res)
                        transformInner(others, acc :+ section, currentLevel)
                    }
                    else {
                        (acc, children)
                    }
                case None =>
                    transformInner(children.tail, acc :+ children.head.detach(), currentLevel)
            }
        }
    }
}

object XmlTransformer {

    val SkipTitleAttr = "skip-title"

}
