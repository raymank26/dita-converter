package com.yandex.money

import java.nio.file.Path

import com.yandex.money.Run.{FactoryService, PathProvider}
import com.yandex.money.config.{CommonConfig, SeparatePagesConfig}
import com.yandex.money.model.ditamap.{ContentType, Reference}
import com.yandex.money.model.{IdentityHref, Link}
import com.yandex.money.processor.DitamapProcessor.DitaMetaInfo
import com.yandex.money.processor.utils.{Cut, IdentityCut, LinkResolver, StaticManager, StaticManagerTrait, WikiDataProvider}

import org.jdom2.Element

/**
 * Converts separate pages only.
 *
 * @author Anton Ermak (ermak@yamoney.ru).
 */
object SeparatePagesRun extends Run {

    override val factoryService: FactoryService = new FactoryService {

        private lazy val pages: SeparatePagesConfig = SeparatePagesConfig.load()

        override lazy val commonConfig: CommonConfig = CommonConfig.load()

        override val pathProvider: PathProvider = new PathProvider {
            override val outputPath: Path = pages.outputPath
            override protected val staticPrefix: String = "images"
        }

        override val imageManager: StaticManagerTrait = StaticManager.apply(pathProvider.staticPath)

        override lazy val wikiDataProvider: WikiDataProvider =
            new WikiDataProviderImpl(WikiDataProvider.Config(commonConfig.cookies,
                ditamapMeta.idToCutType))

        override lazy val linkResolver: LinkResolver =
            new CustomLinkResolver(pages, LinkResolver.Config(pathProvider.outputPath,
                ditamapMeta.nameToHref, ditamapMeta.idToHref, allowLinkErrors = false))
        /**
         * Generates fake .ditamap file
         */
        override lazy val ditamapMeta: DitaMetaInfo = {

            new DitaMetaInfo {

                override val ids: Seq[Int] = pages.pages.map(_.wikiId)

                override val nameToHref: Map[String, String] = pages.links

                override val idToHref: Map[Int, String] = pages.pages.map { page =>
                    page.wikiId -> page.path
                }.toMap

                override val idToContentType: Map[Int, ContentType] =
                    Map.empty.withDefaultValue(Reference)

                override val idToTitle: Map[Int, String] = pages.pages.map { page =>
                    page.wikiId -> page.title
                }.toMap
                override val idToCutType: Map[Int, Cut] = Map().withDefaultValue(IdentityCut)
            }
        }
    }

    private class CustomLinkResolver(separatePages: SeparatePagesConfig, config: LinkResolver.Config)
        extends LinkResolver(config) {

        override protected def isInternal(pageId: Int, link: String): Boolean = {
            val page = separatePages.pages.filter(_.wikiId == pageId).head
            link.startsWith(page.belongsTo) || super.isInternal(pageId, link)
        }

        override protected def resolveById(pageId: Int, link: String): Link = {
            Link(IdentityHref(separatePages.links(link)), Link.Internal, None)
        }
    }

    private class WikiDataProviderImpl(config: WikiDataProvider.Config)
        extends WikiDataProvider(config) {

        override def prepareWikiContent(pageId: Int, content: Element): Element = content
    }

}
