package com.yandex.money.xml;

import org.jdom2.Attribute;
import org.jdom2.Content;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.support.AbstractXMLOutputProcessor;
import org.jdom2.output.support.FormatStack;
import org.jdom2.output.support.Walker;
import org.jdom2.util.NamespaceStack;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * Patched XMLOutputProcessor. Removes "xml:space" attribute from output.
 * @author Anton Ermak (ermak@yamoney.ru).
 */
public class CustomXmlOutputProcessor extends AbstractXMLOutputProcessor {

    @Override
    protected void printElement(Writer out, FormatStack fstack, NamespaceStack nstack, Element
            element) throws
            IOException {

        nstack.push(element);
        try {
            final List<Content> content = element.getContent();

            // Print the beginning of the tag plus attributes and any
            // necessary namespace declarations
            write(out, "<");

            write(out, element.getQualifiedName());

            // Print the element's namespace, if appropriate
            for (final Namespace ns : nstack.addedForward()) {
                printNamespace(out, fstack, ns);
            }

            // Print out attributes
            if (element.hasAttributes()) {
                for (final Attribute attribute : element.getAttributes()) {
                    if (!attribute.getName().equals("space")) {
                        printAttribute(out, fstack, attribute);
                    }
                }
            }

            if (content.isEmpty()) {
                // Case content is empty
                if (fstack.isExpandEmptyElements()) {
                    write(out, "></");
                    write(out, element.getQualifiedName());
                    write(out, ">");
                } else {
                    write(out, " />");
                }
                // nothing more to do.
                return;
            }

            // OK, we have real content to push.
            fstack.push();
            try {

                // Check for xml:space and adjust format settings
                final String space = element.getAttributeValue("space",
                        Namespace.XML_NAMESPACE);

                if ("default".equals(space)) {
                    fstack.setTextMode(fstack.getDefaultMode());
                } else if ("preserve".equals(space)) {
                    fstack.setTextMode(Format.TextMode.PRESERVE);
                }

                // note we ensure the FStack is right before creating the walker
                Walker walker = buildWalker(fstack, content, true);

                if (!walker.hasNext()) {
                    // the walker has formatted out whatever content we had
                    if (fstack.isExpandEmptyElements()) {
                        write(out, "></");
                        write(out, element.getQualifiedName());
                        write(out, ">");
                    } else {
                        write(out, " />");
                    }
                    // nothing more to do.
                    return;
                }
                // we have some content.
                write(out, ">");
                if (!walker.isAllText()) {
                    // we need to newline/indent
                    textRaw(out, fstack.getPadBetween());
                }

                printContent(out, fstack, nstack, walker);

                if (!walker.isAllText()) {
                    // we need to newline/indent
                    textRaw(out, fstack.getPadLast());
                }
                write(out, "</");
                write(out, element.getQualifiedName());
                write(out, ">");

            } finally {
                fstack.pop();
            }
        } finally {
            nstack.pop();
        }
    }
}
