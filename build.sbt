name := "dita-converter"

version := "1.0"

scalaVersion := "2.11.7"

mainClass in assembly := Some("com.yandex.money.Main")

assemblyJarName in assembly := s"${name.value}-${version.value}.jar"

test in assembly := {}

libraryDependencies ++= Seq(
    "org.unbescape" % "unbescape" % "1.1.1.RELEASE",
    "org.scalaj" %% "scalaj-http" % "2.0.0",
    "io.spray" %% "spray-json" % "1.3.2",
    "org.jdom" % "jdom2" % "2.0.6",
    "jaxen" % "jaxen" % "1.1.6",
    "com.typesafe" % "config" % "1.3.0",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
    "ch.qos.logback" % "logback-classic" % "1.1.3",

    "org.scalatest" % "scalatest_2.11" % "2.2.5" % "test"
)