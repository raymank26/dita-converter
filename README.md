# О проекте

Этот проект предназначен для автоматического преобразования ConfluenceWiki-разметки в DITA-разметку.

Процесс работы программы указан на [вики](https://wiki.yamoney.ru/display/API/Tasks.Docs.Editorial).

# Конфигурация

Шаблонный файл конфигурации находится в ```src/main/resources/application_template.conf```.
Для начала работы требуется сделать копию ```application_temlpate.conf``` в ```application.conf``` и
заменить необходимые переменные.
 
Используется библиотека [typesafe-config](https://github.com/typesafehub/config).

# Запуск

Варианта запуска два:

1. Запуск полной конвертации .ditamap документа Яндекс.Кассы - ```sbt "run full"``` (```activator "run full"``` для Windows).
2. Запуск конвертации отдельных страниц - ```sbt run```(```activator run``` для Windows).

В обоих случаях в папке с проектом (в classpath) должен лежать файл `application.conf`
(см. [Standard behavior](https://github.com/typesafehub/config#standard-behavior)).
